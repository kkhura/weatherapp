package com.example.wetherapp;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.example.weatherdata.ui.fragment.WeatherDataFragment;
import com.sapient.environmentlib.Constant.EnvironmentConstants;
import com.sapient.environmentlib.EnvironmentSelectFragment;
import com.sapient.picksourcelib.PickSourceDataFragment;
import com.sapient.picksourcelib.constants.SourceConstants;

public class MainActivity extends AppCompatActivity implements EnvironmentSelectFragment.OnFragmentInteractionListener, PickSourceDataFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            setTitle(getString(R.string.environment));
            getSupportFragmentManager().beginTransaction().add(R.id.frameContainer, EnvironmentSelectFragment.newInstance(), getString(R.string.select_environment_fragment)).commit();
            getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
                @Override
                public void onBackStackChanged() {
                    Fragment frameContainer = getSupportFragmentManager().findFragmentById(R.id.frameContainer);
                    if (frameContainer instanceof EnvironmentSelectFragment) {
                        setTitle(getString(R.string.environment));
                    } else if (frameContainer instanceof PickSourceDataFragment) {
                        String environmentValue = frameContainer.getArguments().getString(EnvironmentConstants.ENVIRONMENT);
                        setTitle(environmentValue.equals(EnvironmentConstants.OWN) ? frameContainer.getArguments().getString(EnvironmentConstants.ENVIRONMENT_NAME) : environmentValue);
                    } else if (frameContainer instanceof WeatherDataFragment) {
                        String source = frameContainer.getArguments().getString(SourceConstants.SOURCE_DATA);
                        setTitle(source);
                    }
                }
            });
        }
    }

    @Override
    public void onFragmentInteraction(Fragment fragment, Bundle bundle) {
        if (fragment instanceof EnvironmentSelectFragment) {
            String environmentValue = bundle.getString(EnvironmentConstants.ENVIRONMENT);
            setTitle(environmentValue.equals(EnvironmentConstants.OWN) ? bundle.getString(EnvironmentConstants.ENVIRONMENT_NAME) : environmentValue);
            getSupportFragmentManager().beginTransaction().add(R.id.frameContainer, PickSourceDataFragment.newInstance(bundle), getString(R.string.pick_up_source_fragment)).addToBackStack(getString(R.string.pick_up_source_fragment)).commit();
        } else if (fragment instanceof PickSourceDataFragment) {
            String source = bundle.getString(SourceConstants.SOURCE_DATA);
            setTitle(source);
            getSupportFragmentManager().beginTransaction().add(R.id.frameContainer, WeatherDataFragment.newInstance(bundle), getString(R.string.weather_data_fragment)).addToBackStack(getString(R.string.weather_data_fragment)).commit();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        hideKeyBoard();
    }

    private void hideKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(this);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

}

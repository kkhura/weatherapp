package com.sapient.environmentlib.Constant;

public class EnvironmentConstants {
    public static final String ENVIRONMENT = "ENVIRONMENT";
    public static final String DEV = "Dev";
    public static final String UAT = "UAT";
    public static final String OWN = "Own";
    public static final String ENVIRONMENT_NAME = "ENVIRONMENT_NAME";
}

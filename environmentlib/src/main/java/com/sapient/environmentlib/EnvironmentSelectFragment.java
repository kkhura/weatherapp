package com.sapient.environmentlib;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;

import com.sapient.environmentlib.Constant.EnvironmentConstants;

public class EnvironmentSelectFragment extends Fragment implements View.OnClickListener {

    private Button btnDev;
    private Button btnUAT;
    private Button btnSubmit;
    private TextInputEditText txtEnvironment;
    private OnFragmentInteractionListener listener;

    public static Fragment newInstance() {
        return new EnvironmentSelectFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setRetainInstance(true);
        return inflater.inflate(R.layout.fragment_environment_select, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnDev = getView().findViewById(R.id.btnDev);
        btnUAT = getView().findViewById(R.id.btnUAt);
        btnSubmit = getView().findViewById(R.id.btnSubmit);
        txtEnvironment = getView().findViewById(R.id.txtEnvironment);

        btnDev.setOnClickListener(this);
        btnUAT.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            listener = (OnFragmentInteractionListener) context;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        hideKeyBoard();
    }

    private void hideKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = getActivity().getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(getActivity());
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onClick(View v) {
        Bundle bundle = new Bundle();
        int i = v.getId();
        if (i == R.id.btnDev) {
            bundle.putString(EnvironmentConstants.ENVIRONMENT, EnvironmentConstants.DEV);
        } else if (i == R.id.btnUAt) {
            bundle.putString(EnvironmentConstants.ENVIRONMENT, EnvironmentConstants.UAT);
        } else if (i == R.id.btnSubmit) {
            String txtEnvironmentValue = txtEnvironment.getText().toString().trim();
            if (txtEnvironmentValue.equals("")) {
                txtEnvironment.setError(getString(R.string.please_enter_environment));
                return;
            } else {
                bundle.putString(EnvironmentConstants.ENVIRONMENT, EnvironmentConstants.OWN);
                bundle.putString(EnvironmentConstants.ENVIRONMENT_NAME, txtEnvironmentValue);
            }
        }
        listener.onFragmentInteraction(this, bundle);
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Fragment fragment, Bundle bundle);
    }
}

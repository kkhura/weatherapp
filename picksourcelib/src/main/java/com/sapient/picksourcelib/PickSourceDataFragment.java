package com.sapient.picksourcelib;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.sapient.picksourcelib.constants.SourceConstants;

public class PickSourceDataFragment extends Fragment implements View.OnClickListener {

    private RadioGroup rgDataSource;
    private OnFragmentInteractionListener listner;

    public static Fragment newInstance(Bundle bundle) {
        PickSourceDataFragment pickSourceDataFragment = new PickSourceDataFragment();
        pickSourceDataFragment.setArguments(bundle);
        return pickSourceDataFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setRetainInstance(true);
        return inflater.inflate(R.layout.fragment_pick_source, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rgDataSource = getView().findViewById(R.id.rgDataSource);
        Button btnGo = getView().findViewById(R.id.btnGo);

        btnGo.setOnClickListener(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            listner = (OnFragmentInteractionListener) context;
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btnGo) {
            int selectedId = rgDataSource.getCheckedRadioButtonId();

            RadioButton rb = getView().findViewById(selectedId);
            Bundle bundle = new Bundle();
            bundle.putString(SourceConstants.SOURCE_DATA, rb.getText().equals(getString(R.string.api)) ? SourceConstants.SOURCE_API : SourceConstants.SOURCE_MOCK);
            listner.onFragmentInteraction(this, bundle);
        }
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Fragment fragment, Bundle bundle);
    }
}

package com.sapient.picksourcelib.constants;

public class SourceConstants {
    public static final String SOURCE_DATA = "SOURCE_DATA";
    public static final String SOURCE_MOCK = "Mock";
    public static final String SOURCE_API = "API";

}

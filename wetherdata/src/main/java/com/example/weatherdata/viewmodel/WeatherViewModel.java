package com.example.weatherdata.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.example.weatherdata.model.WeatherModel;
import com.example.weatherdata.reprository.WeatherReprository;

import java.util.List;

public class WeatherViewModel extends ViewModel {
    private WeatherReprository weatherReprository;
    private MutableLiveData<List<WeatherModel>> responseLiveData;

    public WeatherViewModel(WeatherReprository weatherReprository) {
        this.weatherReprository = weatherReprository;
    }

    public MutableLiveData<List<WeatherModel>> getWeatherData() {
        if (responseLiveData == null) {
            responseLiveData = new MutableLiveData<List<WeatherModel>>();
            weatherReprository.loadWeatherData(responseLiveData);
        }
        return responseLiveData;
    }


    @Override
    protected void onCleared() {
        weatherReprository.clearSubcription();
        super.onCleared();
    }
}
package com.example.weatherdata.viewmodel;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.example.weatherdata.reprository.WeatherReprository;

public class WeatherFactory extends ViewModelProvider.NewInstanceFactory {

    private WeatherReprository weatherReprository;

    public WeatherFactory() {
    }

    public WeatherFactory(WeatherReprository weatherReprository) {
        this.weatherReprository = weatherReprository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new WeatherViewModel(weatherReprository);
    }
}

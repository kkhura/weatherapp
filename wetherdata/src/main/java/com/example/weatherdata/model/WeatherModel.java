package com.example.weatherdata.model;

import android.os.Parcel;
import android.os.Parcelable;

public class WeatherModel implements Parcelable {
    private String timezone;
    private CurrentlyModel currently;

    protected WeatherModel(Parcel in) {
        timezone = in.readString();
        currently = in.readParcelable(CurrentlyModel.class.getClassLoader());
    }

    public static final Creator<WeatherModel> CREATOR = new Creator<WeatherModel>() {
        @Override
        public WeatherModel createFromParcel(Parcel in) {
            return new WeatherModel(in);
        }

        @Override
        public WeatherModel[] newArray(int size) {
            return new WeatherModel[size];
        }
    };

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public CurrentlyModel getCurrently() {
        return currently;
    }

    public void setCurrently(CurrentlyModel currently) {
        this.currently = currently;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(timezone);
        dest.writeParcelable(currently, flags);
    }
}

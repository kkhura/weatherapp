package com.example.weatherdata.model;

import android.os.Parcel;
import android.os.Parcelable;

public class CurrentlyModel implements Parcelable {

    private long time;
    private String summary;
    private float nearestStormDistance;
    private float nearestStormBearing;
    private float precipfloatensity;
    private float precipProbability;
    private float temperature;
    private float apparentTemperature;
    private float dewPofloat;
    private float humidity;
    private float windSpeed;
    private float windGust;
    private float windBearing;
    private float cloudCover;
    private float uvIndex;
    private float visibility;
    private float ozone;
    private float pressure;

    protected CurrentlyModel(Parcel in) {
        time = in.readLong();
        summary = in.readString();
        nearestStormDistance = in.readFloat();
        nearestStormBearing = in.readFloat();
        precipfloatensity = in.readFloat();
        precipProbability = in.readFloat();
        temperature = in.readFloat();
        apparentTemperature = in.readFloat();
        dewPofloat = in.readFloat();
        humidity = in.readFloat();
        windSpeed = in.readFloat();
        windGust = in.readFloat();
        windBearing = in.readFloat();
        cloudCover = in.readFloat();
        uvIndex = in.readFloat();
        visibility = in.readFloat();
        ozone = in.readFloat();
        pressure = in.readFloat();
    }

    public static final Creator<CurrentlyModel> CREATOR = new Creator<CurrentlyModel>() {
        @Override
        public CurrentlyModel createFromParcel(Parcel in) {
            return new CurrentlyModel(in);
        }

        @Override
        public CurrentlyModel[] newArray(int size) {
            return new CurrentlyModel[size];
        }
    };

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public float getNearestStormDistance() {
        return nearestStormDistance;
    }

    public void setNearestStormDistance(float nearestStormDistance) {
        this.nearestStormDistance = nearestStormDistance;
    }

    public float getNearestStormBearing() {
        return nearestStormBearing;
    }

    public void setNearestStormBearing(float nearestStormBearing) {
        this.nearestStormBearing = nearestStormBearing;
    }

    public float getPrecipfloatensity() {
        return precipfloatensity;
    }

    public void setPrecipfloatensity(float precipfloatensity) {
        this.precipfloatensity = precipfloatensity;
    }

    public float getPrecipProbability() {
        return precipProbability;
    }

    public void setPrecipProbability(float precipProbability) {
        this.precipProbability = precipProbability;
    }

    public float getTemperature() {
        return temperature;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    public float getApparentTemperature() {
        return apparentTemperature;
    }

    public void setApparentTemperature(float apparentTemperature) {
        this.apparentTemperature = apparentTemperature;
    }

    public float getDewPofloat() {
        return dewPofloat;
    }

    public void setDewPofloat(float dewPofloat) {
        this.dewPofloat = dewPofloat;
    }

    public float getHumidity() {
        return humidity;
    }

    public void setHumidity(float humidity) {
        this.humidity = humidity;
    }

    public float getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(float windSpeed) {
        this.windSpeed = windSpeed;
    }

    public float getWindGust() {
        return windGust;
    }

    public void setWindGust(float windGust) {
        this.windGust = windGust;
    }

    public float getWindBearing() {
        return windBearing;
    }

    public void setWindBearing(float windBearing) {
        this.windBearing = windBearing;
    }

    public float getCloudCover() {
        return cloudCover;
    }

    public void setCloudCover(float cloudCover) {
        this.cloudCover = cloudCover;
    }

    public float getUvIndex() {
        return uvIndex;
    }

    public void setUvIndex(float uvIndex) {
        this.uvIndex = uvIndex;
    }

    public float getVisibility() {
        return visibility;
    }

    public void setVisibility(float visibility) {
        this.visibility = visibility;
    }

    public float getOzone() {
        return ozone;
    }

    public void setOzone(float ozone) {
        this.ozone = ozone;
    }

    public float getPressure() {
        return pressure;
    }

    public void setPressure(float pressure) {
        this.pressure = pressure;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(time);
        dest.writeString(summary);
        dest.writeFloat(nearestStormDistance);
        dest.writeFloat(nearestStormBearing);
        dest.writeFloat(precipfloatensity);
        dest.writeFloat(precipProbability);
        dest.writeFloat(temperature);
        dest.writeFloat(apparentTemperature);
        dest.writeFloat(dewPofloat);
        dest.writeFloat(humidity);
        dest.writeFloat(windSpeed);
        dest.writeFloat(windGust);
        dest.writeFloat(windBearing);
        dest.writeFloat(cloudCover);
        dest.writeFloat(uvIndex);
        dest.writeFloat(visibility);
        dest.writeFloat(ozone);
        dest.writeFloat(pressure);
    }
}

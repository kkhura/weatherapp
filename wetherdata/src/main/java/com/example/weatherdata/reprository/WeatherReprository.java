package com.example.weatherdata.reprository;

import android.annotation.SuppressLint;
import android.arch.lifecycle.MutableLiveData;
import android.content.res.AssetManager;

import com.example.weatherdata.constants.WeatherDataConstants;
import com.example.weatherdata.model.WeatherModel;
import com.example.weatherdata.reprository.local.GetLocalData;
import com.example.weatherdata.reprository.remote.ApiCallInterface;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiFunction;
import io.reactivex.schedulers.Schedulers;

public class WeatherReprository {
    private ApiCallInterface apiCallInterface;
    private boolean isMockData;
    private AssetManager assetManager;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public WeatherReprository(ApiCallInterface apiCallInterface) {
        this.apiCallInterface = apiCallInterface;
    }

    public WeatherReprository(boolean isMockData, AssetManager assetManager) {
        this.isMockData = isMockData;
        this.assetManager = assetManager;
    }

    public Observable<WeatherModel> getWearherData(String location) {
        return apiCallInterface.getWeatherData(location);
    }

    @SuppressLint("CheckResult")
    public void loadWeatherData(final MutableLiveData<List<WeatherModel>> responseLiveData) {
        if (isMockData) {
            responseLiveData.setValue(new GetLocalData().getListOfData(assetManager));
        } else {
//            zipFunction(responseLiveData);
            mergeFunction(responseLiveData);
//            simpleFunction(responseLiveData);
//            singleFunction(responseLiveData);
        }
    }

    private void singleFunction(final MutableLiveData<List<WeatherModel>> responseLiveData) {
        Single<WeatherModel> wearherData = apiCallInterface.getWeatherDataSingle(WeatherDataConstants.LOC1);

        wearherData.subscribeOn(Schedulers.single())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new SingleObserver<WeatherModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onSuccess(WeatherModel weatherModel) {
                ArrayList<WeatherModel> objects = new ArrayList<>();
                objects.add(weatherModel);
                responseLiveData.setValue(objects);
            }

            @Override
            public void onError(Throwable e) {

            }
        });
    }

    private void simpleFunction(final MutableLiveData<List<WeatherModel>> responseLiveData) {
        Observable<WeatherModel> wearherData = getWearherData(WeatherDataConstants.LOC1);

        wearherData.subscribeOn(Schedulers.single())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<WeatherModel>() {
            @Override
            public void onSubscribe(Disposable d) {
                compositeDisposable.add(d);
            }

            @Override
            public void onNext(WeatherModel weatherModelList) {
                ArrayList<WeatherModel> objects = new ArrayList<>();
                objects.add(weatherModelList);
                responseLiveData.setValue(objects);
            }

            @Override
            public void onError(Throwable e) {
                responseLiveData.setValue(null);
            }

            @Override
            public void onComplete() {

            }
        });
    }


    private void mergeFunction(final MutableLiveData<List<WeatherModel>> responseLiveData) {
        Observable<WeatherModel> wearherData = getWearherData(WeatherDataConstants.LOC1);
        Observable<WeatherModel> wearherData1 = getWearherData(WeatherDataConstants.LOC2);
        Observable<WeatherModel> merge = Observable.merge(wearherData, wearherData1);

        merge.subscribeOn(Schedulers.single())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<WeatherModel>() {
            @Override
            public void onSubscribe(Disposable d) {
                compositeDisposable.add(d);
            }

            @Override
            public void onNext(WeatherModel weatherModelList) {
                ArrayList<WeatherModel> objects = new ArrayList<>();
                objects.add(weatherModelList);
                responseLiveData.setValue(objects);
            }

            @Override
            public void onError(Throwable e) {
                responseLiveData.setValue(null);
            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void zipFunction(final MutableLiveData<List<WeatherModel>> responseLiveData) {
        Observable<WeatherModel> weatherObj1 = getWearherData(WeatherDataConstants.LOC1).subscribeOn(Schedulers.single())
                .observeOn(AndroidSchedulers.mainThread());
        Observable<WeatherModel> weatherObj2 = getWearherData(WeatherDataConstants.LOC2).subscribeOn(Schedulers.single())
                .observeOn(AndroidSchedulers.mainThread());

        Observable.zip(weatherObj1, weatherObj2, new BiFunction<WeatherModel, WeatherModel, List<WeatherModel>>() {
            @Override
            public List<WeatherModel> apply(@NonNull WeatherModel weatherModel1, @NonNull WeatherModel weatherModel2) {
                ArrayList<WeatherModel> weatherModelList = new ArrayList<>();
                if (weatherModel1 != null) {
                    weatherModelList.add(weatherModel1);
                }
                if (weatherModel2 != null) {
                    weatherModelList.add(weatherModel2);
                }
                return weatherModelList;
            }
        }).subscribe(new Observer<List<WeatherModel>>() {
            @Override
            public void onSubscribe(Disposable d) {
                compositeDisposable.add(d);
            }

            @Override
            public void onNext(List<WeatherModel> weatherModelList) {
                if (weatherModelList.size() == 0) {
                    responseLiveData.setValue(null);
                }
                responseLiveData.setValue(weatherModelList);
            }

            @Override
            public void onError(Throwable e) {
                responseLiveData.setValue(null);
            }

            @Override
            public void onComplete() {

            }
        });
    }

    public void clearSubcription() {
        compositeDisposable.dispose();
    }

    /*private void getNextData(final MutableLiveData<List<WeatherModel>> responseLiveData, final ArrayList<WeatherModel> weatherModelList) {
        Call<WeatherModel> call = getWearherData("40.7128,-74.0060");
        call.enqueue(new Callback<WeatherModel>() {
            @Override
            public void onResponse(Call<WeatherModel> call, Response<WeatherModel> response) {
                weatherModelList.add(response.body());
                responseLiveData.setValue(weatherModelList);

            }

            @Override
            public void onFailure(Call<WeatherModel> call, Throwable t) {
                responseLiveData.setValue(null);
            }
        });
    }*/


}
package com.example.weatherdata.reprository.remote;

import com.example.weatherdata.model.WeatherModel;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiCallInterface {
    @GET(Urls.FORECAST)
    Observable<WeatherModel> getWeatherData(@Path(value = "location") String location);

    @GET(Urls.FORECAST)
    Single<WeatherModel> getWeatherDataSingle(@Path(value = "location") String location);
}

package com.example.weatherdata.reprository.local;

import android.content.Context;
import android.content.res.AssetManager;

import com.example.weatherdata.constants.WeatherDataConstants;
import com.example.weatherdata.model.WeatherModel;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class GetLocalData {

    public List<WeatherModel> getListOfData(AssetManager assetManager) {
        ArrayList<WeatherModel> weatherModelList = new ArrayList<>();
        String jsonData = readFromfile(WeatherDataConstants.FILENAME1, assetManager);
        Gson gson = new Gson();
        WeatherModel weatherModel = gson.fromJson(jsonData, WeatherModel.class);
        weatherModelList.add(weatherModel);
        String jsonData1 = readFromfile(WeatherDataConstants.FILENAME2, assetManager);
        Gson gson1 = new Gson();
        WeatherModel weatherModel1 = gson1.fromJson(jsonData1, WeatherModel.class);
        weatherModelList.add(weatherModel1);
        return weatherModelList;
    }

    public String readFromfile(String fileName, AssetManager assetManager) {
        StringBuilder returnString = new StringBuilder();
        InputStream fIn = null;
        InputStreamReader isr = null;
        BufferedReader input = null;
        try {
            fIn = assetManager.open(fileName, Context.MODE_WORLD_READABLE);
            isr = new InputStreamReader(fIn);
            input = new BufferedReader(isr);
            String line = "";
            while ((line = input.readLine()) != null) {
                returnString.append(line);
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (isr != null)
                    isr.close();
                if (fIn != null)
                    fIn.close();
                if (input != null)
                    input.close();
            } catch (Exception e2) {
                e2.getMessage();
            }
        }
        return returnString.toString();
    }
}

package com.example.weatherdata.reprository.remote;

import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class Retrofit {

    private static retrofit2.Retrofit object;

    private Retrofit(){

    }

    public static retrofit2.Retrofit getInstance() {
        if (object == null) {
            synchronized (Retrofit.class) {
                if (object == null) {
                    object = new retrofit2.Retrofit.Builder()
                            .baseUrl(Urls.BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                            .build();
                }
            }
        }
        return object;
    }
}

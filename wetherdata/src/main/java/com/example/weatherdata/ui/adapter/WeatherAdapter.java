package com.example.weatherdata.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.weatherdata.R;
import com.example.weatherdata.model.CurrentlyModel;
import com.example.weatherdata.model.WeatherModel;

import java.util.List;

public class WeatherAdapter extends RecyclerView.Adapter<ViewHolder> {
    private Context context;
    private List<WeatherModel> listWeatherModel;
    private OnClickListner onClickListner;

    public WeatherAdapter(Context context, List<WeatherModel> listWeatherModel, OnClickListner onClickListner) {
        this.context = context;
        this.listWeatherModel = listWeatherModel;
        this.onClickListner = onClickListner;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_weather_info, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        WeatherModel weatherModel = listWeatherModel.get(i);
        viewHolder.txtCity.setText(weatherModel.getTimezone());
        CurrentlyModel currently = weatherModel.getCurrently();
        viewHolder.txtTimeValue.setText(String.valueOf(currently.getTime()));
        viewHolder.txtSummaryValur.setText(currently.getSummary());
        viewHolder.txtTemperatureValue.setText(String.valueOf(currently.getTemperature()));

        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListner.itemClicked(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listWeatherModel.size();
    }

    public interface OnClickListner {
        void itemClicked(int position);
    }
}

class ViewHolder extends RecyclerView.ViewHolder {
    public final TextView txtTimeValue;
    public final TextView txtCity;
    public final TextView txtSummaryValur;
    public final TextView txtTemperatureValue;
    public final CardView cardView;

    public ViewHolder(@NonNull View itemView) {
        super(itemView);
        txtCity = itemView.findViewById(R.id.txtCity);
        txtTimeValue = itemView.findViewById(R.id.txtTimeValue);
        txtSummaryValur = itemView.findViewById(R.id.txtSummaryValue);
        txtTemperatureValue = itemView.findViewById(R.id.txtTemperatureValue);
        cardView = itemView.findViewById(R.id.cardView);
    }
}

package com.example.weatherdata.ui.fragment;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.weatherdata.R;
import com.example.weatherdata.constants.WeatherDataConstants;
import com.example.weatherdata.model.CurrentlyModel;
import com.example.weatherdata.model.WeatherModel;


public class WeatherDetailFragment extends Fragment {

    public static Fragment newInstance(Bundle bundle) {
        WeatherDetailFragment weatherDataFragment = new WeatherDetailFragment();
        weatherDataFragment.setArguments(bundle);
        return weatherDataFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setRetainInstance(true);
        return inflater.inflate(R.layout.fragment_weather_details, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        WeatherModel weatherModel = getArguments().getParcelable(WeatherDataConstants.WEATHER_DATA);

        CurrentlyModel currently = weatherModel.getCurrently();
        ((TextView) getView().findViewById(R.id.txtHumidityValue)).setText(String.valueOf(currently.getHumidity()));
        ((TextView) getView().findViewById(R.id.txtSummaryValue)).setText(currently.getSummary());
        ((TextView) getView().findViewById(R.id.txtTimeValue)).setText(String.valueOf(currently.getTime()));
        ((TextView) getView().findViewById(R.id.txtTemperatureValue)).setText(String.valueOf(currently.getHumidity()));
        ((TextView) getView().findViewById(R.id.txtPressureValue)).setText(String.valueOf(currently.getPressure()));

        getView().findViewById(R.id.cardView).setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void onClick(View v) {
                changeLanguage();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void changeLanguage() {
        /*Resources res = getResources();
        Configuration configuration = getContext().getResources().getConfiguration();
        configuration.setLocale(new Locale("es"));

        res.updateConfiguration(configuration, res.getDisplayMetrics());

        ((BaseActivity) getActivity()).updateScreen();*/
    }
}

package com.example.weatherdata.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.weatherdata.R;
import com.example.weatherdata.constants.WeatherDataConstants;
import com.example.weatherdata.model.WeatherModel;
import com.example.weatherdata.ui.fragment.WeatherDetailFragment;

public class WeatherDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_detail_info);
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            WeatherModel weatherModel = extras.getParcelable(WeatherDataConstants.WEATHER_DATA);
            setTitle(weatherModel.getTimezone());
            getSupportFragmentManager().beginTransaction().add(R.id.frameContainer, WeatherDetailFragment.newInstance(extras), getString(R.string.weather_detail_fragment)).commitAllowingStateLoss();
        }
    }

}

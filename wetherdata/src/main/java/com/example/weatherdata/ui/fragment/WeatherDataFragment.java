package com.example.weatherdata.ui.fragment;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.weatherdata.R;
import com.example.weatherdata.constants.WeatherDataConstants;
import com.example.weatherdata.model.WeatherModel;
import com.example.weatherdata.reprository.WeatherReprository;
import com.example.weatherdata.reprository.remote.ApiCallInterface;
import com.example.weatherdata.reprository.remote.Retrofit;
import com.example.weatherdata.ui.activity.WeatherDetailActivity;
import com.example.weatherdata.ui.adapter.WeatherAdapter;
import com.example.weatherdata.viewmodel.WeatherFactory;
import com.example.weatherdata.viewmodel.WeatherViewModel;
import com.sapient.picksourcelib.constants.SourceConstants;

import java.util.ArrayList;
import java.util.List;


public class WeatherDataFragment extends Fragment implements WeatherAdapter.OnClickListner {

    private WeatherViewModel viewModel;
    private ProgressBar progressBar;
    private RecyclerView rvWeather;
    private WeatherAdapter adapter;
    private ArrayList<WeatherModel> weatherList;

    public static Fragment newInstance(Bundle bundle) {
        WeatherDataFragment weatherDataFragment = new WeatherDataFragment();
        weatherDataFragment.setArguments(bundle);
        return weatherDataFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setRetainInstance(true);
        return inflater.inflate(R.layout.fragment_weather_data, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        progressBar = getView().findViewById(R.id.progress_bar);
        rvWeather = getView().findViewById(R.id.rv_weather);
        if (weatherList == null) {
            weatherList = new ArrayList();
            adapter = new WeatherAdapter(getContext(), weatherList, this);
            rvWeather.setAdapter(adapter);
            rvWeather.setLayoutManager(new LinearLayoutManager(getContext()));

            Bundle arguments = getArguments();

            boolean isMockData = arguments.getString(SourceConstants.SOURCE_DATA).equals(SourceConstants.SOURCE_API) ? false : true;

            WeatherReprository weatherReprository = isMockData ? new WeatherReprository(true, getActivity().getAssets()) : new WeatherReprository(Retrofit.getInstance().create(ApiCallInterface.class));
            WeatherFactory weatherFactory = new WeatherFactory(weatherReprository);

            viewModel = ViewModelProviders.of(this, weatherFactory).get(WeatherViewModel.class);
            viewModel.getWeatherData().observe(this, new Observer<List<WeatherModel>>() {
                @Override
                public void onChanged(@Nullable List<WeatherModel> weatherModel) {
                    if (weatherModel == null) {
                        Toast.makeText(getActivity(),getString(R.string.something_went_wrong),Toast.LENGTH_SHORT).show();
                        progressBar.setVisibility(View.GONE);
                        return;
                    }
                    weatherList.addAll(weatherModel);
                    adapter.notifyDataSetChanged();
                    progressBar.setVisibility(View.GONE);
                }
            });
        }
    }

    @Override
    public void itemClicked(int position) {
        WeatherModel weatherModel = weatherList.get(position);
        getActivity().startActivity(new Intent(getActivity(), WeatherDetailActivity.class).putExtra(WeatherDataConstants.WEATHER_DATA, weatherModel));
    }
}

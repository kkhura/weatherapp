package com.example.weatherdata.constants;

public class WeatherDataConstants {
    public static final String FILENAME1 = "weatherdata.json";
    public static final String FILENAME2 = "weatherdata1.json";
    public static final String LOC1 = "37.3855,-122.088";
    public static final String LOC2 = "40.7128,-74.0060";
    public static String WEATHER_DATA="WEATHER_DATA";
}
